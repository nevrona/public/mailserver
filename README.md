Role Mailserver
=========

An autodeploy for 
[Mailu](https://github.com/Mailu/Mailu).

Requirements
------------

- debian >= 8 
- docker
- docker-compose

Role Variables
--------------
Deployment settings:

```yaml
installation_dir: "/srv/mailserver"
mailu_version: "1.7"
restart_policy: "unless-stopped"
```

Server settings:
```yaml
admin_username: "admin"
admin_password: "MakeMeSecurePlease"
secret_key: "16charRandomstrn"
docker_network_subnet: "172.50.0.0/24"
outbound_enabled: false
outbound_ip: "172.50.0.13"
antivirus_enabled: true
hostnames: "mail.example.org"
domain: "example.org"
auth_ratelimit: "10/minute;1000/hour"
disable_statistics: "False"
```

Mail settings:
```yaml
relaynets: ""
relayhost: ""
compression: ""
compression_level: 
```

Web settings:
```yaml
sitename: "example"
website: "https://example.org"
```
Database settings
```yaml
database_password: "MakeMeSecurePlease"
```

Example playbook
----------------
```yaml
- hosts:  all
  become: yes
  roles:
  - { role: mailserver}
```


Some Notes
----------

Files:
-  [docker-compose.yml](files/docker-compose.yml)
-  [mailu.sample](files/env.sample)

Comes from
[Mailu](https://github.com/hardware/mailserver) and its
license is [MIT](ihttps://github.com/Mailu/Mailu/blob/master/LICENSE.md).

License
-------

Mailu-playbook is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.**

Author Information
-----------------
Facundo Acevedo <facevedo [AT] nevrona.org>
